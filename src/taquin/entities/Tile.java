package taquin.entities;

public class Tile {
    private int value, order;
    private static int count = 0;

    public Tile() {
        count++;
        value = count;
        order = count;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * @return
     */
    public String toString() {
        return String.valueOf(value);
    }
}
