package taquin.entities;

public class EmptyTile {
    private int x;
    private int y;
    /**
     * Instance unique pré-initialisée
     */
    private static EmptyTile instance = new EmptyTile(0, 0);

    /**
     * Point d'accès pour l'instance unique du singleton
     */
    public static EmptyTile getInstance() {
        return instance;
    }

    private EmptyTile(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void set(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


}
