package taquin.entities;

import taquin.heuristic.Heuristic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Taquin {
    private static final Random RANDOM_NUMBER = new Random();
    private Tile[][] tiles;
    private Taquin parent;
    private int dimension;
    private Heuristic heuristic;
    private int g = 0;
    private int h = 0;
    private int f = 0;

    public Taquin(int dimension) {
        this.dimension = dimension;
        tiles = new Tile[dimension][dimension];
    }

    public Taquin(Taquin taquin) {
        dimension = taquin.getDimension();
        heuristic = taquin.getHeuristic();
        g = taquin.getG();
        h = taquin.getH();
        f = taquin.getF();
        tiles = new Tile[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                tiles[i][j] = taquin.tiles[i][j];
            }
        }
    }

    public void setTaquin(Taquin p) {
        dimension = p.getDimension();
        tiles = new Tile[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                tiles[i][j] = p.tiles[i][j];
            }
        }
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    public void setTiles(Tile[][] tiles) {
        this.tiles = tiles;
    }


    public Tile getTile(int x, int y) {
        return tiles[x][y];
    }


    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    /**
     *  On execute l'heuristique.
     * @return
     */
    public int runHeuristic() {
        h =  heuristic.heuristic(this.tiles, this.dimension);
        return h;
    }

    /**
     *
     * @return
     */
    public Heuristic getHeuristic() {
        return heuristic;
    }

    /**
     *
     * @param heuristic
     */
    public void setHeuristic(Heuristic heuristic) {
        this.heuristic = heuristic;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getH() {
        h =  heuristic.heuristic(this.tiles, this.dimension);
        return h;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public Taquin getParent() {
        return parent;
    }

    public void setParent(Taquin parent) {
        this.parent = parent;
    }

    /**
     * Initialisation du taquin.
     */
    public void initTaquin() {
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                Tile c = new Tile();
                if (i == dimension - 1 && j == dimension - 1)
                    c.setValue(0);
                tiles[i][j] = c;
            }
        }
    }



    /**
     * Melange aléatoire du taquin.
     */
    public void shuffleTaquin() {
        for (int i = 0; i < 10; i++) {
            int value = 1 + RANDOM_NUMBER.nextInt(4);
            switch (value) {
                case 1:
                    permutTop();
                    break;
                case 2:
                    permutLeft();
                    break;
                case 3:
                    permutBottom();
                    break;
                case 4:
                    permutRight();
                    break;
            }
        }

    }

    /**
     *  On recupère la case vide.
     * @return
     */
    public EmptyTile getEmptyTile() {
        for (int x = 0; x < dimension; x++) {
            for (int y = 0; y < dimension; y++) {
                if (tiles[x][y].getValue() == 0) {
                    EmptyTile.getInstance().set(x,y);
                    return EmptyTile.getInstance();
                }
            }
        }
        return null;
    }


    public boolean permutTop() {
        Tile tile;
        int x = getEmptyTile().getX();
        int y = getEmptyTile().getY();
        if (x > 0) {
            tile = tiles[x][y];
            tiles[x][y] = tiles[x - 1][y];
            tiles[x - 1][y] = tile;
            return true;
        }
        return false;
    }

    public boolean permutBottom() {
        Tile tile;
        int x = getEmptyTile().getX();
        int y = getEmptyTile().getY();
        if (x < dimension - 1) {
            tile = tiles[x][y];
            tiles[x][y] = tiles[x + 1][y];
            tiles[x + 1][y] = tile;
            return true;
        }
        return false;
    }

    public boolean permutLeft() {
        Tile tile;
        int x = getEmptyTile().getX();
        int y = getEmptyTile().getY();
        if (y > 0) {
            tile = tiles[x][y];
            tiles[x][y] = tiles[x][y - 1];
            tiles[x][y - 1] = tile;
            return true;
        }
        return false;
    }

    public boolean permutRight() {
        Tile tile;
        int x = getEmptyTile().getX();
        int y = getEmptyTile().getY();
        if (y < dimension - 1) {
            tile = tiles[x][y];
            tiles[x][y] = tiles[x][y + 1];
            tiles[x][y + 1] = tile;
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                string.append(String.format("%-5s%-5s", "|", tiles[i][j].getValue()));
            }
            string.append("\n");
        }
        return string.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Taquin)) return false;
        Taquin taquin = (Taquin) o;
        return toString().equals(taquin.toString());
    }

}
