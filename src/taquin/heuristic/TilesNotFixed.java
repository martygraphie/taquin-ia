/*
 * Heuristique :
 * cases non encore en place
 */
package taquin.heuristic;

import taquin.entities.Tile;

public class TilesNotFixed implements Heuristic {

    /**
     *  Méthode de calcul du nombre de case mal placées.
     *
     * @param tiles
     * @param dimension
     * @return
     */
    public int heuristic(Tile[][] tiles, int dimension) {
        int count = 1, n = 0;
        for (int x = 0; x < dimension; x++) {
            for (int y = 0; y < dimension; y++) {
                if (tiles[x][y].getOrder() != count ) {
                    n++;
                }
                count++;
            }
        }
        return n;
    }

	/**
	 *
	 * @return
	 */
    @Override
    public String toString() {
        return "Nombre de cases mal placées ";
    }


}
