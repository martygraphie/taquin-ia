/*
 * Heuristique :
 * sum des distances  Manhattan
 */
package taquin.heuristic;

import taquin.entities.Tile;

public class DistanceManhattan implements Heuristic {

    /**
     *  Méthode de calcul de la somme des distances de Manhattan
     *
     * @param tiles
     * @param dimension
     * @return
     */
    public int heuristic(Tile[][] tiles, int dimension) {
        int sumDistance = 0;
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (tiles[i][j].getValue() !=  0) {
                    int x = (tiles[i][j].getValue() - 1) / dimension;
                    int y = (tiles[i][j].getValue() - 1) % dimension;
                    int distance = Math.abs((i - x)) + Math.abs((j - y));
                    sumDistance = sumDistance + distance;
                }
            }
        }
        return sumDistance;
    }

    public String toString() {
        return "Distance Manhattan";
    }
}
