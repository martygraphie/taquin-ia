package taquin.heuristic;

import taquin.entities.Tile;

public interface Heuristic {
    int heuristic(Tile[][] tiles, int dimension);
}