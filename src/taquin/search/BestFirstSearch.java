package taquin.search;

import taquin.entities.Taquin;

import java.util.ArrayList;

public class BestFirstSearch extends AbstractSearch {

    public BestFirstSearch(Taquin taquin, Taquin originalTaquin) {
        super(taquin, originalTaquin);
    }

    /**
     * Run a star search.
     */
    public void run() {
        initSearch();
        openTaquins.add(taquin);
        while (!resolved) {
            displayWait();
            best = findBetterTiles(openTaquins, openTaquins.get(0));
            displayBestTaquin(best);
            openTaquins.remove(best);
            closedTaquins.add(best);
            if (best.equals(originalTaquin)) {
                resolved = true;
                break;
            }
            for (Taquin nextTaquin : nextTiles(best)) {
                nextTaquin.setParent(best);
                nextTaquin.setF(nextTaquin.getH());
                boolean existInClosedTaquins = containsTiles(closedTaquins, nextTaquin);
                boolean existInOpenTaquins = containsTiles(openTaquins, nextTaquin);
                if (!existInClosedTaquins && !existInOpenTaquins) {
                    openTaquins.add(nextTaquin);
                } else {
                    if (existInClosedTaquins) {
                        for (Taquin closedTaquin : new ArrayList<>(closedTaquins)) {
                            if (closedTaquin.equals(nextTaquin)) {
                                closedTaquins.remove(closedTaquin);
                                openTaquins.add(nextTaquin);
                            }
                        }
                    }
                }
            }
            count++;
        }
        printResult();
    }

    @Override
    public String toString() {
        return "Best First Search";
    }
}