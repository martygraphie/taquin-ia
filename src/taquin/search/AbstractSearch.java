package taquin.search;

import taquin.entities.Taquin;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class AbstractSearch {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_WHITE = "\u001B[37m";
    ArrayList<Taquin> openTaquins = new ArrayList<>();
    ArrayList<Taquin> closedTaquins = new ArrayList<>();
    Taquin taquin;
    Taquin originalTaquin;
    long startTime, startMemory;
    int count;
    boolean resolved = false;
    Taquin best = null;

    public AbstractSearch(Taquin taquin, Taquin originalTaquin) {
        this.taquin = new Taquin(taquin);
        this.originalTaquin = originalTaquin;
    }

    public abstract void run();

    /**
     *
     */
    public void initSearch() {
        System.out.println("Algorithme :  " + toString() + " & Heuristique " + taquin.getHeuristic().toString());
        closedTaquins.clear();
        openTaquins.clear();
        startTime = System.currentTimeMillis();
        startMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        count = 0;
    }

    public Taquin findBetterTiles(ArrayList<Taquin> listTaquins, Taquin best) {
        displayTaquin(best);
        for (int i = 1; i < listTaquins.size(); i++) {
            displayTaquin(listTaquins.get(i));
            if (listTaquins.get(i).getF() < best.getF()) {
                best = listTaquins.get(i);
            }
        }
        return best;
    }

    /**
     * Get next taquin positions.
     *
     * @param taquin
     * @return
     */
    ArrayList<Taquin> nextTiles(Taquin taquin) {
        ArrayList<Taquin> listTaquin = new ArrayList<>();
        if (taquin.permutTop()) {
            listTaquin.add(new Taquin(taquin));
            taquin.permutBottom();
        }
        if (taquin.permutBottom()) {
            listTaquin.add(new Taquin(taquin));
            taquin.permutTop();
        }
        if (taquin.permutLeft()) {
            listTaquin.add(new Taquin(taquin));
            taquin.permutRight();
        }
        if (taquin.permutRight()) {
            listTaquin.add(new Taquin(taquin));
            taquin.permutLeft();
        }
        return listTaquin;
    }

    /**
     * Check if taquin exist in list.
     *
     * @param list
     * @param taquin
     * @return
     */
    boolean containsTiles(final ArrayList<Taquin> list, final Taquin taquin) {
        return list.stream().anyMatch(o -> Arrays.deepEquals(o.getTiles(), taquin.getTiles()));
    }

    public boolean existInAncestor(Taquin taquin) {
        boolean exist = false;
        Taquin current = taquin;
        while (current.getParent() != null){
            current = current.getParent();
            if (current.equals(taquin)) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    public int getPathLength(Taquin taquin) {
        int count = 0;
        Taquin current = taquin;
        while (current.getParent() != null){
            current = current.getParent();
            count++;
        }
        return count;
    }


    /**
     * Display results.
     */
    void printResult() {
        displaySolution(best);
        long stopTime = System.currentTimeMillis();
        long stopMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long elapsedTime = stopTime - startTime;
        long memory = (stopMemory - startMemory) / 1024;

        System.out.println("Memoire utilisée : " + memory + " Ko");
        System.out.println("Temps de calcul de l'algorithme: " + elapsedTime + " millisecondes");
        System.out.println("Nombre de noeud stockés: " + closedTaquins.size());
        System.out.println("Longueur du chemin :" + getPathLength(best));
        System.out.println("=============================================");

    }


    public void displayTaquin(Taquin bestTaquin) {
        System.out.println(bestTaquin.toString() + "h(n) = " + bestTaquin.getF());
    }

    public void displaySolution(Taquin bestTaquin) {
        System.out.println("SOLUTION :");
        System.out.println(bestTaquin.toString() + "h(n) = " + bestTaquin.getF());
    }

    public void displayBestTaquin(Taquin bestTaquin) {
        System.err.println("MEILLEUR : ");
        System.err.println(bestTaquin.toString() + "h(n) = " + bestTaquin.getF());
        System.err.println("=============================================");
    }

    public void displayWait() {
        System.out.println("LISTE D'ATTENTE :");
    }

}
