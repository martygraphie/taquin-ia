package taquin.search;

import taquin.entities.Taquin;

import java.util.ArrayList;

public class AStarSearch extends AbstractSearch {

    public AStarSearch(Taquin taquin, Taquin originalTaquin) {
        super(taquin, originalTaquin);
    }

    /**
     * Run a star search.
     */
    public void run() {
        initSearch();
        openTaquins.add(taquin);
        while (!resolved) {
            displayWait();
            best = findBetterTiles(openTaquins, openTaquins.get(0));
            displayBestTaquin(best);
            openTaquins.remove(best);
            closedTaquins.add(best);
            if (best.equals(originalTaquin)) {
                resolved = true;
                break;
            }
            for (Taquin nextTaquin : nextTiles(best)) {
                nextTaquin.setParent(best);
                nextTaquin.setG(best.getG() + 1);
                nextTaquin.setF(nextTaquin.getG() + nextTaquin.getH());
                boolean existInClosedTaquins = containsTiles(closedTaquins, nextTaquin);
                boolean existInOpenTaquins = containsTiles(openTaquins, nextTaquin);
                if (!existInClosedTaquins && !existInOpenTaquins) {
                    openTaquins.add(nextTaquin);
                } else {
                    if (existInOpenTaquins) {
                        for (Taquin openTaquin : new ArrayList<>(openTaquins)) {
                            if (openTaquin.equals(nextTaquin)) {
                                if (openTaquin.getF() >= nextTaquin.getF()) {
                                    openTaquins.remove(openTaquin);
                                    openTaquins.add(nextTaquin);
                                }
                            }
                        }
                    }
                    if (existInClosedTaquins) {
                        for (Taquin closedTaquin : new ArrayList<>(closedTaquins)) {
                            if (closedTaquin.equals(nextTaquin)) {
                                if (closedTaquin.getF() >= nextTaquin.getF()) {
                                    closedTaquins.remove(closedTaquin);
                                    openTaquins.add(nextTaquin);
                                }
                            }
                        }
                    }
                }
            }
            count++;
        }
        printResult();
    }

    @Override
    public void displayTaquin(Taquin taquin) {
        System.out.println(taquin.toString() + "h(n) = " + taquin.getH() + " + g(n) = " + taquin.getG());
    }

    @Override
    public void displaySolution(Taquin bestTaquin) {
        System.out.println("SOLUTION :");
        System.out.println(taquin.toString() + "h(n) = " + taquin.getH() + " + g(n) = " + taquin.getG());
    }

    @Override
    public void displayBestTaquin(Taquin taquin) {
        System.err.println("MEILLEUR : ");
        System.err.println(taquin.toString() + "h(n) = " + taquin.getH() + " + g(n) = " + taquin.getG());
    }

    @Override
    public String toString() {
        return "A*";
    }
}
