package taquin.search;

import taquin.entities.Taquin;

import java.util.Iterator;

public class HillClimbingSearch extends AbstractSearch {

    public HillClimbingSearch(Taquin taquin, Taquin originalTaquin) {
        super(taquin, originalTaquin);
    }

    /**
     * Run best first search.
     */
    public void run() {
        initSearch();
        boolean resolved = false;
        openTaquins.add(taquin);
        while (!openTaquins.isEmpty() && !resolved) {
            best = findBetterTiles(openTaquins, openTaquins.get(0));
            displayBestTaquin(best);
            openTaquins.clear();
            if (best.equals(originalTaquin)) {
                resolved = true;
            }
            for (Taquin nextTaquin : nextTiles(best)) {
                nextTaquin.setParent(best);
                nextTaquin.setF(nextTaquin.getH());
                if (!existInAncestor(nextTaquin)) {
                    openTaquins.add(nextTaquin);
                }
            }
            count++;
        }
        printResult();
    }

    @Override
    public String toString() {
        return "Hill Climbing Search";
    }

}
