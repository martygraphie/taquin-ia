import taquin.entities.Tile;
import taquin.heuristic.TilesNotFixed;
import taquin.search.AStarSearch;
import taquin.search.AbstractSearch;
import taquin.search.BestFirstSearch;
import taquin.search.HillClimbingSearch;
import taquin.entities.Taquin;
import taquin.heuristic.DistanceManhattan;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        System.out.println("\n");
        System.out.println("Selectionner la taille de votre taquin :");
        System.out.println("-------------------------\n");
        System.out.println("1 - 3 x 3");
        System.out.println("2 - 4 x 4");
        System.out.println("3 - 5 x 5");
        Scanner scanner = new Scanner(System.in);
        int dimension = scanner.nextInt();
        while (dimension < 0 || dimension > 3) {
            System.out.println("Votre choix ne fait pas partie de la liste, recommencer");
            dimension = scanner.nextInt();
        }

        Taquin taquin = new Taquin(dimension + 2);
        taquin.initTaquin();
        System.out.println("\n");
        System.out.println("Selectionner votre algorithme :");
        System.out.println("-------------------------\n");
        System.out.println("1 - Alogirthme Hill Climbing");
        System.out.println("2 - Algorithme Best First");
        System.out.println("3 - Algorithme A*");
        System.out.println("4 - Tous les algorithmes");
        System.out.println("5 - Quitter");
        scanner = new Scanner(System.in);
        int algorithme = scanner.nextInt();
        while (algorithme < 0 || algorithme > 5) {
            System.out.println("Votre choix ne fait pas partie de la liste, recommencer");
            algorithme = scanner.nextInt();
        }
        System.out.println("Option " + algorithme + " selectionnée");
        System.out.println("\n");
        if (algorithme != 5) {
            System.out.println("Selectionner votre heuristique :");
            System.out.println("-------------------------\n");
            System.out.println("1 - Distance de Manhattan");
            System.out.println("2 - Nombre de case mal placées");
            System.out.println("3 - Toutes les heuristiques");
            int heuristic = scanner.nextInt();
            while (heuristic < 0 || heuristic > 3) {
                System.out.println("Votre choix ne fait pas partie de la liste, recommencer");
                heuristic = scanner.nextInt();
            }
            System.out.println("Option " + heuristic + " selectionnée");
            System.out.println("\n");
            switch (heuristic) {
                case 1:
                case 3:
                    taquin.setHeuristic(new DistanceManhattan());
                    break;
                case 2:
                    taquin.setHeuristic(new TilesNotFixed());
                    break;
            }
            System.out.println("Taquin départ : ");
            System.out.println(taquin.toString());
            Taquin originalTaquin = new Taquin(taquin);
            taquin.shuffleTaquin();
            while (taquin.equals(originalTaquin)) {
                taquin.shuffleTaquin();
            }
            System.out.println("Taquin mélangé : ");
            System.out.println(taquin.toString());
            taquin.setF(taquin.getH());
            AbstractSearch algorithmeSearch;
            switch (algorithme) {
                case 1:
                    algorithmeSearch = new HillClimbingSearch(taquin, originalTaquin);
                    algorithmeSearch.run();
                    if (heuristic == 3) {
                        taquin.setHeuristic(new TilesNotFixed());
                        algorithmeSearch = new HillClimbingSearch(taquin, originalTaquin);
                        algorithmeSearch.run();
                    }
                    break;
                case 2:
                    algorithmeSearch = new BestFirstSearch(taquin, originalTaquin);
                    algorithmeSearch.run();
                    if (heuristic == 3) {
                        taquin.setHeuristic(new TilesNotFixed());
                        algorithmeSearch = new BestFirstSearch(taquin, originalTaquin);
                        algorithmeSearch.run();
                    }
                    break;
                case 3:
                    algorithmeSearch = new AStarSearch(taquin, originalTaquin);
                    algorithmeSearch.run();
                    if (heuristic == 3) {
                        taquin.setHeuristic(new TilesNotFixed());
                        algorithmeSearch = new AStarSearch(taquin, originalTaquin);
                        algorithmeSearch.run();
                    }
                    break;

                case 4:
                    algorithmeSearch = new HillClimbingSearch(taquin, originalTaquin);
                    algorithmeSearch.run();
                    algorithmeSearch = new BestFirstSearch(taquin, originalTaquin);
                    algorithmeSearch.run();
                    algorithmeSearch = new AStarSearch(taquin, originalTaquin);
                    algorithmeSearch.run();

                    if (heuristic == 3) {
                        taquin.setHeuristic(new TilesNotFixed());
                        algorithmeSearch = new HillClimbingSearch(taquin, originalTaquin);
                        algorithmeSearch.run();
                        algorithmeSearch = new BestFirstSearch(taquin, originalTaquin);
                        algorithmeSearch.run();
                        algorithmeSearch = new AStarSearch(taquin, originalTaquin);
                        algorithmeSearch.run();
                    }
                    break;

                default:
            }
        }
    }

}
