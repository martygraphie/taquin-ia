# TAQUIN GAMES ARTIFICIAL INTELLIGENCE

## Presentation
The realized project is the game of the taquin (1 player). This game consists of a board in which there is a defined number of squares (3 x 3 ; 4 x 4 ; 5 x 5). Each full square contains a number except for one empty square. 

It will allow the full squares to exchange places. At the beginning of the game, the order of the squares will be randomly shuffled. The goal of the game is to put the numbers back in the order below.

However, the user can only move one full square at a time and this square will have to take the place of the empty square. 
The objective of this project is to automate the resolution of the game by a computer program. 

To carry out the project, Java technology was chosen, the teaser is rendered in console.
The user has the possibility to choose the algorithms and the heuristics they want to use. The teaser is initially designed in 3 x 3. 



## Algorithms
To solve the teaser problem, several algorithms have been experimented with. 

### Hill Climbing search
To solve the Hill Climbing search, an initial situation is generated, it goes through all its neighbors and keeps the one with the best heuristic.
Then the operation must be repeated until a position better than the neighboring positions is reached. If a position is equal to the final solution then the algorithm stops.

### Best First search
To solve the Best First search, two lists of nodes are initialized:
* the list of nodes waiting to be processed
* the list of processed nodes.

The Best First algorithm determines which node to place in the list of processed nodes by comparing the evaluation functions of the nodes in the list waiting to be processed. An evaluation function is calculated by adding the cost of the heuristic with the cost of moving from the starting situation. 

The best node is added to the list of processed nodes and removed from the queue
If the neighboring node is equal to the solution, the algorithm ends.
Next, the search goes through the set of nodes neighboring the best node. 

If there is no occurrence of the neighbor in either list, it is added to the queue.

If an occurrence of the neighbor exists in the list of processed nodes, then the occurrence is removed from the list of processed nodes and the neighbor is added to the list of nodes waiting to be processed.
Thus, a node that has already been traversed may well be re-evaluated.

### A* search 
The A* search method is almost identical to Best First. The main difference is that the evaluation function takes into account the cost of moving from the starting situation. This cost must be taken into account in all three conditions. 

If there is no occurrence of the neighbor in one of the two lists, it is added to the queue.

If an occurrence of the neighbor exists in the list of nodes waiting to be processed and its evaluation function is greater than or equal to that of the current neighbor, then the occurrence is removed from the queue and the neighbor is added.

If an occurrence of the neighbor exists in the list of processed nodes and its evaluation function is greater than or equal to that of the current neighbor, then the occurrence is deleted from the list of processed nodes and the neighbor is added to the list of nodes waiting to be processed.

#### Heuristics
Two heuristics were performed and tested in order to compare their impact on the different algorithms: 

### Manhattan distance
This method calculates the distance of a cell from the final position. A counter is initialized to zero, it represents the total distance of the boxes. Then, we check for each square if they are well positioned. If a square is not well placed, we calculate the distance between the current square and the position of the final square and we add it to the counter otherwise we do nothing. Once all the squares have been covered, the counter is returned.

### Number of misplaced cells 
This method calculates the number of misplaced squares on the teaser. A counter is initialized to zero, it represents the number of misplaced squares. Then we check for each square if they are well placed. If a square is misplaced, we increment the counter, otherwise we do nothing. Once all the positions have been traversed, the counter is returned.

